
/*
// Когда расширение устанавливается или обновляется.
chrome.runtime.onInstalled.addListener(function() {
  // Replace all rules ...
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    // With a new rule ...
    chrome.declarativeContent.onPageChanged.addRules([
      {
        // That fires when a page's URL contains a 'g' ...
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { hostContains: "vk.com", urlContains: "w=wall-" }
          })
        ],
        // And shows the extension's page action.
        actions: [ new chrome.declarativeContent.ShowPageAction() ]
      }
    ]);
  });
});
*/

function showPopup (){
  if(currentLead && currentLead.type){
    console.info ('popup');
  }
}

// Обработчик на обновление вкладки. Общий для всех сайтов.
chrome.tabs.onUpdated.addListener(function(id,info,tab)
{
  
  if (info.status == "complete") {

    handleTab(tab);
  }
  
});

// Обработчик на активацию вкладки.  Общий для всех сайтов.
chrome.tabs.onActivated.addListener(function(activeInfo)
{
  chrome.tabs.get(activeInfo.tabId, function(tab){
    handleTab(tab);
  });
});

// Фильтр и распределение по обработчикам сайтов.
function handleTab (tab){
  
  // По url отбрабатываем только нужные.
  var url = tab.url;
  console.log('url = ' + url);
  
  if(url.indexOf('www.partzilla.com/product/') !== -1){
    
    return handlePartzillaProduct(tab);
  }

  if(url.indexOf('https://vk.com') !== -1){
    
    return handleVKPage(tab);
  }
  
  if(url.indexOf('https://vk.com') !== -1){
    
    return checkForLead(tab);
  }
}

// Обработчик страницы partzilla с запчастью.
function handleVKPage(tab){

  var d = {
    type: 'vk_page',
    data: {}
  };
  
  chrome.tabs.sendMessage(tab.id, d);
}

// Обработчик страницы partzilla с запчастью.
function handlePartzillaProduct(tab){

  var d = {
    type: 'partzilla_part',
    data: {}
  };
  
  chrome.tabs.sendMessage(tab.id, d);
}


function checkForLead(tab){
  var url = tab.url;
  
  var pid;	
  var s = url.split("w=wall");
  if (s.length == 2)
  {
    pid = s[1];
  } else {
    return;
  }
    
  console.log('pid = ' + pid);
  if (pid  !=  "")
  {
    $.ajax({
      type: "GET",
      url: "https://api.vk.com/method/wall.getById",
      dataType: "json",
      data: {
        posts: pid,
        lang: 'ru',
        extended: '1'
      },
      success: function(response){
        console.log('Получили пост ВК');
        
        var uid = response.response.wall[0].from_id;
                
        var user = getVKUser(uid);
        var country = getVKCountry(user.country);
        var city = getVKCity(user.city);
        
        var d = {
          type:'lead_vk',
          data:{
            VK_URL: url,
            Text: response.response.wall[0].text,
            Contact_First_Name: user.first_name,
            Contact_Second_Name: user.last_name,
            Contact_Mob_Phone: user.mobile_phone,
            Contact_Home_Phone: user.home_phone,
            Country: country,
            City: city
            
          }
        };
        
        chrome.tabs.sendMessage(tab.id,d);
        
      },
      error: function(xhr, status){
        console.log(response+': ' + xhr);
      }
    });
  }
}

function getVKUser(uid){
  $.ajax({
    type: "GET",
    url: "https://api.vk.com/method/getProfiles",
    dataType: "json",
    data: {
      uids: uid,
      lang: 'ru',
      fields: 'first_name, last_name, city, country, contacts'
    },
    success: function(response){
      console.log('Получили пользователя ВК');

      return response.response;
    },
    error: function(xhr, status){
      console.log(response+': ' + xhr);
    }
  });
}

function getVKCountry(id){
  $.ajax({
    type: "GET",
    url: "https://api.vk.com/method/getCountries",
    dataType: "json",
    data: {
      cids: id,
      lang: 'ru'
    },
    success: function(response){
      console.log('Получили Страну');

      return response.response.name;
    },
    error: function(xhr, status){
      console.log(response+': ' + xhr);
    }
  });
}
    
function getVKCity(id){
  $.ajax({
    type: "GET",
    url: "https://api.vk.com/method/getCities",
    dataType: "json",
    data: {
      cids: id,
      lang: 'ru'
    },
    success: function(response){
      console.log('Получили Город');

      return response.response.name;
    },
    error: function(xhr, status){
      console.log(response+': ' + xhr);
    }
  });
}
    
    