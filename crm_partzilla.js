$(document).ready(function(){
  
  
  
  // Обработчик сообщений от бэка.
//  chrome.extension.onMessage.addListener(function(r)
//  {
    
    // Реагируем только на объекты типа "Запчасть на partzilla"
//    if(typeof r=='object' && r.type == 'partzilla_part') {
      
      // Добавляем раздел zoho с кнопками, если его еще нет.
      var zoho = $('#zoho');
      if (zoho.length == 0)
      {
        // Добавляем раздел zoho, если его еще нет.
        // Ищем форму form_add_cart и добавляем туда.
        var f = $('form#form_add_cart');
        if(f.length == 0)
        {
          console.info('Нет формы id form_add_cart');
          alert('Нет формы id form_add_cart. Нужно править код!');
          return;
        }
        
        zoho = $("<div></div>").attr("id", "zoho").addClass('row');
        f.append(zoho);
      }
      
      // Делаем кнопку, по которой открывается предзаполненная форма zoho.
      btn = $("<button></button>").addClass('btn btn-primary').text("Добавить запчасть");
      btn.click(function(){
        
        // Собираем параметры предзаполения.
        var enName = $("meta[itemprop='name']").attr('content');
        var partNumber = $("meta[itemprop='sku']").attr('content');
        var manufacturer = $("meta[itemprop='manufacturer']").attr('content');
        
        // Отдельно парсим раздел Подходит для.
        var fitment  = '';
        var fitmentTab = $('#RFTab2');
        if(fitmentTab.length == 1)
        {
          
          var a = fitmentTab.find('a');
          if (a.length > 0){
            
            a.each(function (index, element) {
              
              var val = element.text;
              if(manufacturer == 'Yamaha'){
                
                val = val.replace('Yamaha Motorcycle ', '');
              }
              
              if(index > 0){
                fitment += '\n';
              }
              
              fitment += val;
              
            });
            
          } else {
            alert('В разделе Подходит для не найдены ссылки. Нужно править код!');
          }
          
          
        } else {
          alert('Не найден раздел Подходит для. Нужно править код!');
        }
        
        var data = {
          OEM: 'true',
          Part_Number: partNumber,
          Name_EN: enName,
          Make_Name: manufacturer,
          Fitment: fitment
        };
        
          var zoho_url = "https://creator.zohopublic.com/voltrun/motocycle-parts/form-perma/Moto_Parts_BASE/5tfd6r3Ut9UND2bCaqx4apxjxtZs0zXqfrvxMJTCze3Xk16yQzFT82RP0pyqhsJ4aOwTkb4PWAdpdEXBrEhEKOfBRbfwKTSPje0W?"+$.param( data );
          window.open(zoho_url, '_blank');
        });
      zoho.append(btn);
//    }
//  });
});